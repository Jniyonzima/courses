# -*- mode: org -*-

#+TITLE: Storing data in variables
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

[[file:images/c1_u03_variables.png]]

----- 

* Introduction

We use variables in programming as places to temporarily store data so
that we can manipulate that data. We can have the user write to it via
the keyboard, we can use files to write data to variables, we can do math
operations or modify these variables, and we can print them back out to the
screen or a text file.

Variables can store many different types of data, with the most basic types
being *integers* (whole numbers) *floats* (numbers with decimal portions),
*strings* ("text within double quotes"), *characters* (a single number, letter, or symbol, usually stored in 's'ingle 'q'uotes),
and *booleans* (true/false).

-----

* Types of data

The term *data type* refers to the type of data that is stored within a variable.
For some programming languages, the data type is figured out via context and
we don't have to specify what the *type* is in our code (Python, Lua),
but with statically typed languages, we must *declare* a variable and its type
before using it.

Some of the most basic *data types* and example values are:

| Data type | Description                                  | C++ example            | Python example | Lua example    |
|-----------+----------------------------------------------+------------------------+----------------+----------------|
| integer   | whole numbers                                | =int age=100;=         | =age=100=      | =age=100=      |
| float     | numbers w/ decimals                          | =float price=9.95;=    | =price=9.95=   | =price=9.95=   |
| boolean   | true or false                                | =bool saved=true;=     | =saved=True=   | =saved=true=   |
| string    | text, numbers, symbols, within double quotes | =string name==\quot{}asdf\quot{}= |

In C++, *string* values must be within double quotes and *characters* within single quotes:

#+BEGIN_SRC cpp :class cpp
  string name="Pixel";
  char currency='$';
#+END_SRC

And Python and Lua can work with *characters* but generally you work with everything as a *string*, so you can write your strings in double quotes /or/ single quotes:


#+BEGIN_SRC python :class python
  name="Pixel"
  name='Pixel'
#+END_SRC

-----

* Creating variables

*Assignment statements* allow us to store data within a variable. It takes this form:

=VARIABLENAME = DATA=

In Python and Lua you can create a variable by simply writing an *assignment statement*, creating a new variable name
(or reusing an existing one), and setting it equal to some data...

#+BEGIN_SRC python :class python
  print( "I need a variable to store how much money I have..." )
  my_money = 123.45
  print( "I have", my_money )
#+END_SRC


In C++, variables must be *declared* before or while being *assigned*, and their *data type* must be specified, and
the statement will end with a semicolon =;=

- Declare without assignment :: =TYPE VARIABLENAME;=
- Declare with assignment :: =TYPE VARIABLENAME = DATA;=
- Assignment (after declaration) :: =VARIABLENAME = DATA;=

#+BEGIN_SRC cpp :class cpp
  cout << "I need a variable to store how much money I have..." << endl;
  float my_money = 123.45;
  cout << "I have " << my_money << endl;
#+END_SRC

* Assigning values to variables

Once a variable has been declared (in C++) or assigned a value (Python/Lua), then it will continue existing
in that context. You can keep using the variable, such as for usage in math operations,
and you can overwrite the value being stored in that variable as well.

When assigning a *value* to a *variable*, the variable being assigned to always goes on the *left-hand side ("LHS")* of the equal sign.
Here, the equal sign = is known as the *assignment operator*.
On the *right-hand side ("RHS")* of the equal sign you can use a *literal* to assign a specific value,
or include an expression, such as a math operation, whose result will then be stored in the variable.

- Python/Lua assignment statement :: =VARIABLENAME = VALUE=
- C++ assignment statement :: =VARIABLENAME = VALUE;=

** Assigning literal values to variables

[[file:images/c1_u03_assignment.png]]

A literal is an actual number or string that you're hard-coding into the program, such as below:

| Description                                        | Python/Lua         | C++                 |
|----------------------------------------------------+--------------------+---------------------|
| Assigning a float literal to the =price= variable  | =price = 9.76=     | =price = 9.76;=     |
| Assigning a string literal to the =state= variable | =state = "Kansas"= | =state = "Kansas";= |

** Copying data between variables

[[file:images/c1_u03_copy.png]]

If you have variables on both sides of the equal sign, then the value from the RHS gets copied to the variable on the LHS:

| Description                                       | Python/Lua              | C++                      |
|---------------------------------------------------+-------------------------+--------------------------|
| Copy value from =book_price= to price variable    | =price = book_price=    | =price = book_price;=    |
| Copy value from =college_state= to state variable | =state = college_state= | =state = college_state;= |


** Storing the result of a math operation in a variable

[[file:images/c1_u03_expression.png]]

The RHS may have a mathematical expression. The program will compute the result of that operation and store the result in the variable on the LHS.

| Description                                                   | Python/Lua                | C++                        |
|---------------------------------------------------------------+---------------------------+----------------------------|
| Evaluate math expression and store result in =price= variable | =price = price1 + price2= | =price = price1 + price2;= |

You can even do math on a variable and assign the result back to the same variable...

| Description                                                        | Python/Lua            | C++                    |
|--------------------------------------------------------------------+-----------------------+------------------------|
| Add 1 to =counter= and store the result in the =counter= variable. | =counter = counter+1= | =counter = counter+1;= |


*Python*:
#+BEGIN_SRC python :class python
  cat_count = 1
  print( "I have", cat_count, "cat(s)" )
  print( "..." )
  print( "Oops, adopted another cat!" )
  cat_count = cat_count + 1
  print( "I have", cat_count, "cat(s)" )
#+END_SRC

*C++*:
#+BEGIN_SRC cpp :class cpp
  int cat_count = 1
  cout << "I have", cat_count, "cat(s)" << endl;
  cout << "..." << endl;
  cout << "Oops, adopted another cat!" << endl;
  cat_count = cat_count + 1;
  cout <<  "I have", cat_count, "cat(s)" << endl;
#+END_SRC

-----

* Named constants

A named constant looks similar to a variable in that it is a place to store data and it has a name,
however the *value* of a named constant cannot (or should not) change after its initialization.
With C++, this is enforced by using the =const= keyword, though with Lua and Python, the language itself
does not enforce the /const/-ness.

| Description                         | Python/Lua           | C++                         |
|-------------------------------------+----------------------+-----------------------------|
| Create named constant =VERSION_NUM= | =VERSION_NUM = 1.23= | =const VERSION_NUM = 1.23;= |

Named constanst are useful because we may have some data we want to use in the program in various places,
but if we hard-coded that value (like =1.23=), we would then have to update /every occurrence of that number/
if we were going to change it. Additionally, someone unfamiliar with that section of code might go, "1.23? What does that mean??".
By using *named constants*, you can refer to it throughout the program by name - which is descriptive of what it /is/ - and
when you need to update it, you just update one location - the original declaration.

-----

* Naming conventions

You can name a variable anything you’d like - but it can only contain numbers,
underscore =_=, and upper- and lower-case letters in the name. Variable
names can begin with the underscore or a letter, but it cannot start with a
number. And definitely NO spaces allowed in a variable name!

Additionally, a variable name cannot be a *keyword* - a name
reserved for something else in the language, such as =if=, =int=, etc.

Most programming languages, including C++, Python, and Lua, are case sensitive as well, which means if you name
a variable username, it will be a different variable from one named =userName=
(or if you type =userName= when the variable is called =username=, the compiler/interpreter
will complain at you because it doesn’t know what you mean).

- Python ::
  - Variables: You should use =lower_case_with_underscores= for variables
  - Named constants: Give =FULLY_UPPER_CASE= names
  - Documentation: https://peps.python.org/pep-0008/#function-and-variable-names
- C++ :: There is no "official" style, and you may encounter =lower_case_with_underscores= style or =camelCaseWords= style. The main thing is to be consistent.
  - Variables: Use =lower_case_with_underscores= or =camelCase=
  - Named constants: Use =FULLY_UPPER_CASE=
  - Documentation: https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#nl8-use-a-consistent-naming-style
- Lua :: There doesn't seem to be a specific naming convention, but as with above, just be consistent.
  - Variables: Use =lower_case_with_underscores= or =camelCase=
  - Named constants: Use =FULLY_UPPER_CASE=
  - Documentation: https://lua-users.org/wiki/LuaStyleGuide

-----

* Working with variables

As you build a program you will be utilizing variables in different ways...

** Display variable values

We can use our language's display functionality to display the value of a variable.
When we do this, it is good to include a *label* before the variable's value so the user knows what is being communicated.

| Description                        | Python/Lua                 | C++                                   |
|------------------------------------+----------------------------+---------------------------------------|
| Display value of variable =price=  | =print( price )=           | =cout << price;=                      |
| Display label and value of =price= | =print( "Price:", price )= | =cout << "Price: " << price << endl;= |


** Storing user input in a variable

We may also ask the user to enter some data, which we will then store within a variable. For types like *integers*
and *floats*, this can be pretty simple. With *Python*, we have the =input= function, which allows us to display a question or prompt
/before/ the user enters their selection. For *Lua* and *C++*, we will need to use a display command (print/cout) to display the question or prompt /first/,
and then get the user's input /second/.


*Getting input in Python*:
#+BEGIN_SRC python :class python
  name = input( "What is the name of the product? " )
  quantity = int( input( "How many products do we have? " ) )
  price = float( input( "What is the price of the product? " ) )
#+END_SRC

With Python, =input= gives us a =string= by default. To make sure we're getting
an integer, we need to /wrap/ the =input( )= function with the =int( )= function.
Similarly, for floats, we need to surround =input( )= with the =float( )= function.

*Getting input in Lua*:
#+BEGIN_SRC lua :class lua
  print( "What is the name of the product?" )
  name = io.read( "*l" )
  print( "How many products do we have?" )
  quantity = io.read( "*n" )
  print( "What is the price of the product?" )
  price = io.read( "*n" )
#+END_SRC

With Lua, we use the =io.read= function. Within the double quotes, we specify what type of information
we are reading. =*l= means "line", and will allow us to read in text with spaces.
=*n= means "number", and will allow us to read integers and floats.

*Getting input in C++*:
#+BEGIN_SRC cpp :class cpp
  string name;
  cout << "What is the name of the product? ";
  getline( cin, name );

  int quantity;
  cout << "How many products do we have? ";
  cin >> quantity;

  float price;
  cout << "What is the price of the product? ";
  cin >> price;
#+END_SRC

With C++, we use =cin= to get input from the console. For lines of strings, we need to use
the special =getline( )= function, with =cin= as the first item and our variable name as the second.
With integers and floats, we can use =cin >>= to read values into those variables.

We will cover more with getting input from the keyboard in a later lesson.


** Performing math operations

We can perform math computations with our variables and with numeric literals. The most common operations are:

| Operation      | Symbol | Example   |
|----------------+--------+-----------|
| Addition       | =+=    | =z = x+y= |
| Subtraction    | =-=    | =z = x-y= |
| Multiplication | =*=    | =z = x*y= |
| Division       | =/=    | =z = x/y= |

*Make sure to put the result somewhere!* When you do a math operation and you want to use the result
elseware in the program, make sure
you’re storing the result in a variable via an assignment statement! If you
just do this, nothing will happen:

#+BEGIN_SRC cpp :class cpp
  total_cats + 1;
#+END_SRC

You can use an assignment statement to store the result in a new variable...

#+BEGIN_SRC cpp :class cpp
  new_total = total_cats + 1;
#+END_SRC

Or overwrite the variable you’re working with...

#+BEGIN_SRC cpp :class cpp
  total_cats = total_cats + 1;
#+END_SRC

*** Compound operations

Compound operations: There are also shortcut operations you can use
to quickly do some math and overwrite the original variable. This works with
each of the arithmetic operations:

*In C++:*

#+BEGIN_SRC cpp :class cpp
  total_cats = total_cats + 1;
  total_cats += 1;
  total_cats++;
  ++total_cats;
#+END_SRC

Each of these add 1 to the =total_cats= variable. With the top two lines, we could change =1= to be any other number to
add multiple to the variable.

*In Python:*
#+BEGIN_SRC python :class python
  total_cats = total_cats + 1
  total_cats += 1
#+END_SRC

Python doesn't support the =++= operation to add 1 to a variable, and Lua doesn't support that nor the =+== command.

In addition to =+== to add onto a variable, we can also use =-==
to subtract a value,
=*== to multiply a value,
and =/== to divide a value.


** Working with text strings

If we have two variables storing =string= values and we use the =+= operator on them, we will *concatenate* them -
or combine them - together, in *C++* and *Python*:

#+BEGIN_SRC cpp :class cpp
  flavor = "chocolate";
  dessert = "ice cream";
  food = flavor + " " + dessert;
#+END_SRC

With the above, we will get the result ="chocolate ice cream"= stored in the =food= variable.
This works the same with Python, except you don't need the semicolons =;= at the end of the statements.

For *Lua*, the =..= operator is used to concatenate:

#+BEGIN_SRC lua :class lua
  flavor = "chocolate"
  dessert = "cake"
  food = flavor .. " " .. dessert
#+END_SRC

