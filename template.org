# -*- mode: org -*-

#+TITLE: CS 235/250 Unit 06 Exercise: Debugging and testing
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Introduction

- About ::
- Goals ::
  - Debugging
- Setup ::

-----


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
bash
#+END_SRC

- Python :: 
#+BEGIN_SRC python :class python
#+END_SRC

- Lua :: 
#+BEGIN_SRC lua :class lua
#+END_SRC

- C++ :: 
#+BEGIN_SRC cpp :class cpp
#+END_SRC

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* asdfasdf
#+END_HTML

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* asdfasdf
#+END_HTML


#+BEGIN_QUOTE
Quote thing
#+END_QUOTE



#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
LEFT
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML
RIGHT
#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML




#+ATTR_HTML: :border 2 :rules all :frame border :class col-7 highlight-col-6 :style width: 100%;
| Value: | "Cats" | "Dogs" | "Mice" | "Birds" | "Ferrets" | "" |
| Index: |      0 |      1 |      2 |       3 |         4 |  5 |


#+begin_src artist

=^o.o^=
    
#+end_src





# LaTeX ONLY - Display code with formatting. Hide in the CSS.
# #+ATTR_HTML: :class invisible
# #+NAME: content
# #+BEGIN_HTML

# \begin{lstlisting}[style=code]
#   cout << arr[2];
# \end{lstlisting}

# #+END_HTML


# #+ATTR_HTML: :class invisible
# #+NAME: content
# #+BEGIN_HTML
# \iftoggle{exportlatex}{ LaTeX }{ HTML }
# #+END_HTML

# I want to hide this when exporting for LaTeX, but how??
#+BEGIN_SRC cpp :class cpp latexhide
  cout << arr[2];
#+END_SRC




-----
