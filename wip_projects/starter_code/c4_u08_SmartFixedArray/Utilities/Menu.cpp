#include "Menu.h"

namespace Utility
{

bool Menu::s_lastCinWasStream = false;

//! Uses cin >> to get a string
std::string Menu::CinStreamString()
{
    std::string buffer;
    std::cin >> buffer;

    Menu::s_lastCinWasStream = true;

    return buffer;
}

//! Uses cin to get an integer
int Menu::CinStreamInt()
{
    int buffer;
    std::cin >> buffer;

    Menu::s_lastCinWasStream = true;

    return buffer;
}

//! Uses getline to get a string
std::string Menu::CinGetlineString()
{
    std::string buffer;
    if ( Menu::s_lastCinWasStream )
    {
        std::cin.ignore();
    }

    getline( std::cin, buffer );
    Menu::s_lastCinWasStream = false;

    return buffer;
}

//! Display an accented header
void Menu::Header( const std::string& header )
{
    DrawHorizontalBar( 80 );
    std::string head = "| " + header + " |";
    std::cout << " " << head << std::endl << " ";
    DrawHorizontalBar( head.size() );
    std::cout << std::endl;
}

//! Draw a horizontal rule
void Menu::DrawHorizontalBar( int width, char symbol )
{
    std::cout << std::string( width, symbol ) << std::endl;
}

//! Display a menu to the screen, formatted nicely
void Menu::ShowMenu( const std::vector<std::string> options, bool vertical, bool zeroToQuit /*= false*/ )
{
    if ( vertical )
    {
        if ( zeroToQuit )
        {
            std::cout << " 0.\tQuit" << std::endl;
        }
        for ( unsigned int i = 0; i < options.size(); i++ )
        {
            std::cout << " " << (i+1) << ".\t" << options[i] << std::endl;
        }
    }
    else
    {
        if ( zeroToQuit )
        {
            std::cout << " 0.\tQuit\t";
        }
        for ( unsigned int i = 0; i < options.size(); i++ )
        {
            std::cout << " " << (i+1) << ". " << options[i] << "\t";
        }
        std::cout << std::endl;
    }
}

//! Display a menu of options to the screen and get the user's selection, return as an integer.
int Menu::ShowIntMenuWithPrompt( const std::vector<std::string> options, bool vertical /*= true*/, bool zeroToQuit /*= false*/ )
{
    ShowMenu( options, vertical, zeroToQuit );
    int minVal = ( zeroToQuit ) ? 0 : 1;
    int choice = GetValidChoice( minVal, options.size() );
    return choice;
}

//! Display a menu of options to the screen and get the user's selection, return as a string.
std::string Menu::ShowStringMenuWithPrompt( const std::vector<std::string> options, bool vertical )
{
    ShowMenu( options, vertical );
    int choice = GetValidChoice( 1, options.size() );
    std::string value = options[ choice-1 ];
    return value;
}

/*
 * Call looks like this:

Menu::ShowCallbackMenuWithPrompt( {
    { "Set text", std::bind( &Program::Menu_SetText, this ) },
    { "Get text", std::bind( &Program::Menu_GetText, this ) }
} )();
*
 * */
//! Display a menu of options to the screen, a function pointer is returned as the result.
std::function<void(void)> Menu::ShowCallbackMenuWithPrompt( std::map<std::string, std::function<void(void)> > options, bool vertical )
{
    std::vector<std::string> menuOptions;
    for ( auto& opt : options )
    {
        menuOptions.push_back( opt.first );
    }

    ShowMenu( menuOptions, vertical );
    int choice = GetValidChoice( 1, options.size() );
    std::string key = menuOptions[ choice-1 ];

    return options[ key ];
}

//! Ask the user to enter a number, ensures that number is between min and max before returning it.
int Menu::GetValidChoice( int min, int max, const std::string& message )
{
    if ( message != "" )
    {
        std::cout << std::endl;
        DrawHorizontalBar( message.size() + 2 );
        std::cout << " " << message << std::endl;
    }

    int choice = GetIntChoice();

    while ( choice < min || choice > max )
    {
        std::cout << "Invalid selection. Try again." << std::endl;
        choice = GetIntChoice();
    }

    return choice;
}

//! Asks the user to enter a string, returns the input.
std::string Menu::GetStringChoice( const std::string& message )
{
    if ( message != "" )
    {
        std::cout << " " << message << std::endl;
    }

    std::cout << std::endl << " >> ";
    std::string choice = Menu::CinStreamString();
    std::cout << std::endl;
    return choice;
}

//! Asks the user to enter a string line, returns the input.
std::string Menu::GetStringLine( const std::string& message )
{
    if ( message != "" )
    {
        std::cout << " " << message << std::endl;
    }
    std::cout << std::endl << " >> ";
    std::string choice = Menu::CinGetlineString();
    std::cout << std::endl;
    return choice;
}

//! Asks the user to enter an integer, returns the input.
int Menu::GetIntChoice( const std::string& message )
{
    if ( message != "" )
    {
        std::cout << " " << message << std::endl;
    }
    std::cout << std::endl << " >> ";
    int choice = Menu::CinStreamInt();
    std::cout << std::endl;
    return choice;
}

// HANDY TRICKS
//! Clears the screen
void Menu::ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

//! Supposed to ask the user to hit ENTER to continue but doesn't always work properly on every system.
void Menu::Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        std::cout << std::endl << " Press ENTER to continue..." << std::endl;
        if ( Menu::s_lastCinWasStream )
        {
            std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
        }
        else
        {
            std::cin.get();
        }
    #endif
}

//! Prints the Present Working Directory
void Menu::PrintPwd()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "echo %cd%" );
    #else
        system( "pwd" );
    #endif
}

} // End of namespace
