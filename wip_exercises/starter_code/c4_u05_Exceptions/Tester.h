#ifndef _TESTER
#define _TESTER

#include "Functions.h"

void RunTests();
void Test_Divide();
void Test_Display();
void Test_PtrDisplay();
void Test_SlicesPerPerson();

#endif
