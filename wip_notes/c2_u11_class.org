# -*- mode: org -*-

1. What is a class?
2. What is an object?
3. What is an access modifier?
4. What is public?
5. What is private?
6. What is a member variable?
7. What is a member function?
8. How do you tell a member variable from a parameter variable when inside of a class' function?
9. How do you declare a class? (Write down example code)
10. How do you declare an object variable of some class type? (Write down example code)
11. What kind of file do class declarations go in?
12. What kind of file do class function definitions go in?
13. What does a class function definition function header look like? (it's a little different from a non-class function) 

