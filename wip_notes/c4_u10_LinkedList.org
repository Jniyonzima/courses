# -*- mode: org -*-

Write down the function implementation for each of the following. Adding diagrams will also help
you recall the information later on.

- PushFront
- PushBack
- PushAt
- PopFront
- PopBack
- PopAt
- GetFront
- GetBack
- GetAt

