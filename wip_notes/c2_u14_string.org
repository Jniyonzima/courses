# -*- mode: org -*-

1. Note the C++ documentation page for strings: https://cplusplus.com/reference/string/string/
2. C++ strings are sequences of what kind of data type?
3. What function is used to get the amount of characters in a string? Write out some example code for how to use it.
4. What operator is used to get a character at a certain index of a string? Write out some example code for how to use it.
5. What operator is used to add on (concatenate) to the end of a string? Write out some example code for how to use it.
6. What function is used to insert some text into the middle of a string? Write out some example code for how to use it.
7. What function is used to erase some text in a string? Write out some example code for how to use it.
8. What function is used to replace some text at some position in a string? Write out some example code for how to use it.
9. What function is used to see if some text is in a string? Write out some example code for how to use it. 
